import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import importedData from "./Static/data.json";

const getData: any = () => {
  const storage = localStorage.getItem("data");
  const staticData = Object.values(importedData);
  if (!storage) return staticData;
  const json = JSON.parse(storage);
  const jsonVec = Object.values(json);
  if (jsonVec.length > 0) return jsonVec;
  return staticData;
};

const data = getData();

ReactDOM.render(
  <React.StrictMode>
    <App data={data} />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
