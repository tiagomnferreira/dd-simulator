const SIZES = {
  1: 1,
  5: 5,
  10: 10,
};

const PADDING = {
  1: SIZES[1],
};

const MARGIN = {
  5: SIZES[5],
  10: SIZES[10],
};

export { PADDING, MARGIN };
