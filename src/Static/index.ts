import COLORS from "./colors";
import _ from "lodash";

const getColor = (color?: string) => {
  if (!color) return false;
  return _.get(COLORS, `${color.toUpperCase()}`);
};

export { getColor };
