import styled from "styled-components";

const Wrapper = styled.div`
  min-width: 300px;
  margin-left: 1rem;
`;

export { Wrapper };
