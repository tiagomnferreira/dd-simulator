import React from "react";
import {
  Container,
  Header,
  Body,
  HeaderCell,
  Cell,
  Row,
  Footer,
} from "./styled-components";
import { getColor } from "../../Static";

type PropTypes = {
  drugs: Drug[];
};

const calcSales = (sold: number, price: number) => sold * price;

const calcProfit = (sold: number, sell: number, bought: number, buy: number) =>
  calcSales(sold, sell) - calcSales(bought, buy);

const calcTotalBought = (drugs: Drug[]) =>
  drugs.reduce((acc: number, drug: Drug) => acc + drug.bought, 0);

const calcTotalSold = (drugs: Drug[]) =>
  drugs.reduce((acc: number, drug: Drug) => acc + drug.sold, 0);

const calcTotalSales = (drugs: Drug[]) =>
  drugs.reduce(
    (acc: number, drug: Drug) => acc + calcSales(drug.sold, drug.sell),
    0
  );

const calcTotalProfit = (drugs: Drug[]) =>
  drugs.reduce(
    (acc: number, drug: Drug) =>
      acc + calcProfit(drug.sold, drug.sell, drug.bought, drug.buy),
    0
  );

const calcTotalStock = (drugs: Drug[]) =>
  drugs.reduce((acc: number, drug: Drug) => acc + drug.stock, 0);

const calcStockLevel = (drug: Drug) => {
  if (drug.stock < 10) return getColor("red");
  if (drug.stock < 50) return getColor("yellow");
  return getColor("gree");
};

const printCells = (drugs: Drug[]) =>
  drugs.map((drug: Drug, index) => (
    <Row key={index}>
      <Cell color={getColor(drug.color).toString()}>{drug.text}</Cell>
      <Cell>{drug.buy}$/g</Cell>
      <Cell>{drug.sell}$/g</Cell>
      <Cell>{drug.bought}g</Cell>
      <Cell>{drug.sold}g</Cell>
      <Cell>{calcSales(drug.sold, drug.sell)}$</Cell>
      <Cell>{calcProfit(drug.sold, drug.sell, drug.bought, drug.buy)}$</Cell>
      <Cell color={calcStockLevel(drug)}>{drug.stock}g</Cell>
    </Row>
  ));

const Table = ({ drugs }: PropTypes) => (
  <Container>
    <Header>
      <Row>
        <HeaderCell></HeaderCell>
        <HeaderCell>Buy ($/g)</HeaderCell>
        <HeaderCell>Sell ($/g)</HeaderCell>
        <HeaderCell>Bought (g)</HeaderCell>
        <HeaderCell>Sold (g)</HeaderCell>
        <HeaderCell>Sales ($)</HeaderCell>
        <HeaderCell>Profit ($$)</HeaderCell>
        <HeaderCell>Stock (g)</HeaderCell>
      </Row>
    </Header>
    <Body>{printCells(drugs)}</Body>
    <Footer>
      <Row>
        <Cell>Total</Cell>
        <Cell></Cell>
        <Cell></Cell>
        <Cell>{calcTotalBought(drugs)}g</Cell>
        <Cell>{calcTotalSold(drugs)}g</Cell>
        <Cell>{calcTotalSales(drugs)}$</Cell>
        <Cell>{calcTotalProfit(drugs)}$</Cell>
        <Cell>{calcTotalStock(drugs)}g</Cell>
      </Row>
    </Footer>
  </Container>
);

export default React.memo(Table);
