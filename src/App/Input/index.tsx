import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { Dropdown, IDropdownOption } from "office-ui-fabric-react/lib/Dropdown";
import {
  DefaultButton,
  PrimaryButton,
  IIconProps,
  initializeIcons,
} from "office-ui-fabric-react";
import {
  Stack,
  IStackProps,
  IStackStyles,
} from "office-ui-fabric-react/lib/Stack";
import { Container } from "./styled-components";

initializeIcons();

type PropTypes = {
  drugs: Drug[];
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>;
};

const addIcon: IIconProps = { iconName: "Add" };
const minusIcon: IIconProps = { iconName: "Remove" };

const stackStyles: Partial<IStackStyles> = {
  root: { width: 650, alignItems: "center", display: "flex" },
};

const columnProps: Partial<IStackProps> = {
  styles: { root: { width: 400 } },
};

const buy = (
  value: string,
  selected: Drug,
  drugs: Drug[],
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>
) => {
  const mapped = drugs.map((drug: Drug) => {
    if (drug.key === selected.key) {
      const parsed = parseInt(value.length === 0 ? "0" : value);
      const bought = drug.bought + parsed;
      const stock = drug.stock + parsed;
      const newDrug = {
        ...drug,
        stock,
        bought,
      };
      return newDrug;
    }
    return drug;
  });
  setDrugs(mapped);
};

const sell = (
  value: string,
  selected: Drug,
  drugs: Drug[],
  setDrugs: React.Dispatch<React.SetStateAction<Drug[]>>
) => {
  const mapped = drugs.map((drug: Drug) => {
    if (drug.key === selected.key) {
      const parsed = parseInt(value.length === 0 ? "0" : value);
      const sold = drug.sold + parsed;
      const stock = drug.stock - parsed;
      if (stock >= 0) {
        const newDrug = {
          ...drug,
          stock,
          sold,
        };
        return newDrug;
      }
      alert("Not enough stock");
    }
    return drug;
  });
  setDrugs(mapped);
};

const checkIfDisabled = (selectedDrug: Drug | undefined, drugs: Drug[]) => {
  if (selectedDrug) {
    const found = drugs.find((drug: Drug) => drug.key === selectedDrug.key);
    return found && found.stock <= 0 ? true : false;
  }
  return true;
};

const handleDropdown = (
  item: IDropdownOption | undefined,
  setSelectedDrug: React.Dispatch<React.SetStateAction<Drug | undefined>>
): void => setSelectedDrug(item as Drug);

const handleInput = (
  newValue: string,
  setValue: React.Dispatch<React.SetStateAction<string>>
) => setValue(newValue);

const Input = ({ drugs, setDrugs }: PropTypes) => {
  const [selectedDrug, setSelectedDrug] = useState<Drug | undefined>();
  const [value, setValue] = useState<string>("");

  return (
    <Container>
      <Stack horizontal tokens={{ childrenGap: 10 }} styles={stackStyles}>
        <Dropdown
          options={drugs}
          placeholder="select a drug"
          styles={{ root: { width: 150 } }}
          selectedKey={selectedDrug ? selectedDrug.key : undefined}
          onChange={(event, item) => handleDropdown(item, setSelectedDrug)}
        />
        <Stack {...columnProps}>
          <TextField
            placeholder="grams"
            type="number"
            value={value}
            onChange={(e, newValue) => {
              if (newValue) handleInput(newValue, setValue);
            }}
          />
        </Stack>
        <Stack tokens={{ childrenGap: 5 }}>
          <PrimaryButton
            iconProps={addIcon}
            text="buy"
            disabled={selectedDrug ? false : true}
            onClick={() => {
              if (selectedDrug) {
                buy(value, selectedDrug, drugs, setDrugs);
                setValue("");
              }
            }}
          />
          <DefaultButton
            iconProps={minusIcon}
            text="sell"
            disabled={checkIfDisabled(selectedDrug, drugs)}
            onClick={() => {
              if (selectedDrug) {
                sell(value, selectedDrug, drugs, setDrugs);
                setValue("");
              }
            }}
          />
        </Stack>
      </Stack>
    </Container>
  );
};

export default React.memo(Input);
